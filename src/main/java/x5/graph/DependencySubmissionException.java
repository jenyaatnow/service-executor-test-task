package x5.graph;

public class DependencySubmissionException extends RuntimeException {

    public DependencySubmissionException(String message) {
        super(message);
    }
}
