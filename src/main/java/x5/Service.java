package x5;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Service {

    private final String serviceName;
    private final Long timeout;

    private boolean currentlyVisited;
    private boolean previouslyVisited;

    private Set<Service> dependencies = new HashSet<>();

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS");
    private static final String PATTERN = "[%s - %s] %s %s";

    public Service(final String serviceName, final Long timeout) {
        this.serviceName = serviceName;
        this.timeout = timeout;
    }

    public void run() {
        final String threadName = Thread.currentThread().getName();
        printMessage("start", threadName);

        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        printMessage("end", threadName);
    }

    private void printMessage(final String message, final String threadName) {
        final String date = DATE_FORMAT.format(new Date());
        final String startMessage = String.format(PATTERN, threadName, date, serviceName, message);
        System.out.println(startMessage);
    }

    public boolean isCurrentlyVisited() {
        return currentlyVisited;
    }

    public void setCurrentlyVisited(boolean currentlyVisited) {
        this.currentlyVisited = currentlyVisited;
    }

    public boolean isNotPreviouslyVisited() {
        return !previouslyVisited;
    }

    public void setPreviouslyVisited(boolean previouslyVisited) {
        this.previouslyVisited = previouslyVisited;
    }

    public Set<Service> getDependencies() {
        return dependencies;
    }

    public void setDependencies(Set<Service> dependencies) {
        this.dependencies = dependencies;
    }
}
