package x5.graph;

public class GraphCycleException extends RuntimeException {

    public GraphCycleException(String message) {
        super(message);
    }
}
