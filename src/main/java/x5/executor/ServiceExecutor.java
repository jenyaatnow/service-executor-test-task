package x5.executor;

import x5.Service;
import x5.graph.ServiceGraphValidator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class ServiceExecutor {

    private final ServiceGraphValidator serviceGraphValidator;
    private ExecutorService executorService;
    private Map<Service, CompletableFuture<?>> futureMap;

    public ServiceExecutor(final ServiceGraphValidator serviceGraphValidator) {
        this.serviceGraphValidator = serviceGraphValidator;
    }

    public void execute(final int threadCount, final Set<Service> serviceGraph) {
        if (serviceGraph.isEmpty()) return;
        final Set<Service> serviceGraphCopy = new HashSet<>(serviceGraph);
        performValidation(serviceGraph);

        futureMap = new HashMap<>();
        executorService = Executors.newFixedThreadPool(threadCount);

        runMainLoop(serviceGraphCopy);

        futureMap.values().forEach(CompletableFuture::join);
        executorService.shutdown();
    }

    private void runMainLoop(final Set<Service> serviceGraph) {
        while (!serviceGraph.isEmpty()) {
            final Set<Service> readyToScheduleServices = getServicesWithResolvedDependencies(serviceGraph);

            readyToScheduleServices.forEach((service) -> {
                final CompletableFuture<?>[] dependencyFutures = getDependencyFutures(service.getDependencies());
                final CompletableFuture<Void> future = CompletableFuture
                    .allOf(dependencyFutures)
                    .thenRunAsync(service::run, executorService);

                futureMap.put(service, future);
                serviceGraph.remove(service);
            });
        }
    }

    private Set<Service> getServicesWithResolvedDependencies(final Set<Service> serviceGraph) {
        return serviceGraph
            .stream()
            .filter(it -> futureMap.keySet().containsAll(it.getDependencies()))
            .collect(Collectors.toSet());
    }

    private CompletableFuture<?>[] getDependencyFutures(final Set<Service> dependencies) {
        return futureMap.entrySet()
            .stream()
            .filter(futureEntry -> dependencies.contains(futureEntry.getKey()))
            .map(Map.Entry::getValue)
            .toArray(CompletableFuture[]::new);
    }

    private void performValidation(final Set<Service> serviceGraph) {
        serviceGraphValidator.validateCyclicDependencies(serviceGraph);
        serviceGraphValidator.validateDependencySubmission(serviceGraph);
    }
}
