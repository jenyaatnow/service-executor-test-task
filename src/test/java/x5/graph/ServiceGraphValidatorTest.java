package x5.graph;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import x5.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class ServiceGraphValidatorTest {

    private static final long TIMEOUT = 100;

    private ServiceGraphValidator serviceGraphValidator;

    @BeforeEach
    void setup() {
        serviceGraphValidator = new ServiceGraphValidator();
    }

    @Test
    void validateSelfDependency() {
        final Service s1 = new Service("s1", TIMEOUT);

        final Set<Service> servicesGraph = new HashSet<>(Collections.singletonList(s1));
        s1.setDependencies(new HashSet<>(Collections.singleton(s1)));
        
        Assertions.assertThrows(
            GraphCycleException.class,
            () -> serviceGraphValidator.validateCyclicDependencies(servicesGraph)
        );
    }

    @Test
    void validateCycleDependencyBetweenTwoServices() {
        final Service s1 = new Service("s1", TIMEOUT);
        final Service s2 = new Service("s2", TIMEOUT);

        final Set<Service> servicesGraph = new HashSet<>(Arrays.asList(s1, s2));
        s1.setDependencies(new HashSet<>(Collections.singleton(s2)));
        s2.setDependencies(new HashSet<>(Collections.singleton(s1)));

        Assertions.assertThrows(
            GraphCycleException.class,
            () -> serviceGraphValidator.validateCyclicDependencies(servicesGraph)
        );
    }

    @Test
    void validateTransitiveCycleDependency() {
        final Service s1 = new Service("s1", TIMEOUT);
        final Service s2 = new Service("s2", TIMEOUT);
        final Service s3 = new Service("s3", TIMEOUT);

        final Set<Service> servicesGraph = new HashSet<>(Arrays.asList(s1, s2, s3));
        s1.setDependencies(new HashSet<>(Collections.singleton(s2)));
        s2.setDependencies(new HashSet<>(Collections.singleton(s3)));
        s3.setDependencies(new HashSet<>(Collections.singleton(s1)));

        Assertions.assertThrows(
            GraphCycleException.class,
            () -> serviceGraphValidator.validateCyclicDependencies(servicesGraph)
        );
    }

    @Test
    void validateNoCycleDependency() {
        final Service s1 = new Service("s1", TIMEOUT);
        final Service s2 = new Service("s2", TIMEOUT);
        final Service s3 = new Service("s3", TIMEOUT);
        final Service s4 = new Service("s4", TIMEOUT);

        final Set<Service> servicesGraph = new HashSet<>(Arrays.asList(s1, s2, s3, s4));

        s1.setDependencies(new HashSet<>(Arrays.asList(s2, s3)));
        s2.setDependencies(Collections.singleton(s4));
        s3.setDependencies(Collections.singleton(s4));

        Assertions.assertDoesNotThrow(() -> serviceGraphValidator.validateCyclicDependencies(servicesGraph));
    }

    @Test
    void validateMissingDependencySubmission() {
        final Service s1 = Mockito.spy(new Service("s1", TIMEOUT));
        final Service s2 = Mockito.spy(new Service("s2", TIMEOUT));
        final Service s3 = Mockito.spy(new Service("s3", TIMEOUT));

        final Set<Service> servicesGraph = Collections.singleton(s1);
        s1.setDependencies(Collections.singleton(s2));
        s2.setDependencies(Collections.singleton(s3));

        Assertions.assertThrows(
            DependencySubmissionException.class,
            () -> serviceGraphValidator.validateDependencySubmission(servicesGraph)
        );
    }

    @Test
    void validateExplicitlySubmittedDependencies() {
        final Service s1 = Mockito.spy(new Service("s1", TIMEOUT));
        final Service s2 = Mockito.spy(new Service("s2", TIMEOUT));
        final Service s3 = Mockito.spy(new Service("s3", TIMEOUT));

        final Set<Service> servicesGraph = new HashSet<>(Arrays.asList(s1, s2, s3));
        s1.setDependencies(Collections.singleton(s2));
        s2.setDependencies(Collections.singleton(s3));

        Assertions.assertDoesNotThrow(() -> serviceGraphValidator.validateDependencySubmission(servicesGraph));
    }
}
