package x5.graph;

import x5.Service;

import java.util.Set;
import java.util.stream.Collectors;

public class ServiceGraphValidator {

    public void validateDependencySubmission(final Set<Service> serviceGraph) {
        final Set<Service> dependencies = serviceGraph.stream()
            .flatMap(service -> service.getDependencies().stream())
            .collect(Collectors.toSet());

        if (serviceGraph.containsAll(dependencies)) return;
        throw new DependencySubmissionException("Each dependency should be submitted explicitly");
    }

    public void validateCyclicDependencies(final Set<Service> serviceGraph) {
        serviceGraph.forEach(service -> {
            if (service.isNotPreviouslyVisited() && hasCycle(service)) {
                throw new GraphCycleException("Cyclic dependencies found");
            }
        });
    }

    private boolean hasCycle(final Service service) {
        service.setCurrentlyVisited(true);

        final boolean hasCycle = service.getDependencies()
            .stream()
            .anyMatch(dependency -> {
                if (dependency.isCurrentlyVisited()) {
                    return true;
                } else {
                    return dependency.isNotPreviouslyVisited() && hasCycle(dependency);
                }
            });

        service.setCurrentlyVisited(false);
        service.setPreviouslyVisited(true);
        return hasCycle;
    }
}
