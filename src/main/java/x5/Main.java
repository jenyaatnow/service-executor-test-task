package x5;

import x5.executor.ServiceExecutor;
import x5.graph.ServiceGraphValidator;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        final Service s1 = new Service("s1", 2000L);
        final Service s2 = new Service("s2", 2000L);
        final Service s3 = new Service("s3", 2000L);
        final Service s4 = new Service("s4", 2000L);
        final Service s5 = new Service("s5", 2000L);
        final Service s6 = new Service("s6", 2000L);

        final Set<Service> servicesGraph = new HashSet<>(Arrays.asList(s1, s2, s3, s4, s5, s6));

        s1.setDependencies(new HashSet<>(Arrays.asList(s2, s3)));
        s2.setDependencies(Collections.singleton(s4));
        s3.setDependencies(Collections.singleton(s4));
        s6.setDependencies(Collections.singleton(s1));

        final ServiceExecutor serviceExecutor = new ServiceExecutor(new ServiceGraphValidator());
        serviceExecutor.execute(4, servicesGraph);
    }
}
