package x5.executor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InOrder;
import org.mockito.Mockito;
import x5.Service;
import x5.graph.ServiceGraphValidator;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class ServiceExecutorTest {

    private static final long TIMEOUT = 100;

    private ServiceExecutor serviceExecutor;
    private ServiceGraphValidator serviceGraphValidator;

    @BeforeEach
    void setup() {
        serviceGraphValidator = Mockito.mock(ServiceGraphValidator.class);
        serviceExecutor = new ServiceExecutor(serviceGraphValidator);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4})
    void testExecutionOrder(final int threadCount) {
        final Service s1 = Mockito.spy(new Service("s1", TIMEOUT));
        final Service s2 = Mockito.spy(new Service("s2", TIMEOUT));
        final Service s3 = Mockito.spy(new Service("s3", TIMEOUT));

        final Set<Service> servicesGraph = new HashSet<>(Arrays.asList(s1, s2, s3));
        s1.setDependencies(Collections.singleton(s2));
        s2.setDependencies(Collections.singleton(s3));

        final InOrder inOrder = Mockito.inOrder(s1, s2, s3);

        serviceExecutor.execute(threadCount, servicesGraph);

        inOrder.verify(s3).run();
        inOrder.verify(s2).run();
        inOrder.verify(s1).run();
        inOrder.verifyNoMoreInteractions();
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4})
    void eachServiceShouldRunExactlyOnce(final int threadCount) {
        final Service s1 = Mockito.spy(new Service("s1", TIMEOUT));
        final Service s2 = Mockito.spy(new Service("s2", TIMEOUT));
        final Service s3 = Mockito.spy(new Service("s3", TIMEOUT));

        final Set<Service> servicesGraph = new HashSet<>(Arrays.asList(s1, s2, s3));
        s1.setDependencies(new HashSet<>(Arrays.asList(s2, s3)));
        s2.setDependencies(Collections.singleton(s3));

        final InOrder inOrder = Mockito.inOrder(s1, s2, s3);

        serviceExecutor.execute(threadCount, servicesGraph);

        inOrder.verify(s3).run();
        inOrder.verify(s2).run();
        inOrder.verify(s1).run();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    void validationShouldBePerformed() {
        final Service s1 = new Service("s1", TIMEOUT);
        final Set<Service> servicesGraph = Collections.singleton(s1);

        serviceExecutor.execute(4, servicesGraph);

        Mockito.verify(serviceGraphValidator).validateCyclicDependencies(servicesGraph);
        Mockito.verify(serviceGraphValidator).validateDependencySubmission(servicesGraph);
    }
}
